Для установки проекта необходимо выполнить следующие команды:
```
git clone https://gitlab.com/gionov/fabrique_test.git
cd fabrique_test
docker-compose build
docker-compose up
```


При первом запуске необходимо выполнить следующие команды:
```
docker-compose run --rm web-app sh -c "python manage.py makemigrations"
docker-compose run --rm web-app sh -c "python manage.py migrate"
docker-compose run --rm web-app sh -c "python manage.py createsuperuser"
```


Для того, чтобы не прописывать такие длиные команды, можно воспользоваться терминалом сервиса web-app внутри Docker Desctop.
В таком случае команды будут выглядеть следующим образом:
```
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser
```


Выполненные дополнительные задания:
1. организовать тестирование написанного кода
3. подготовить docker-compose для запуска всех сервисов проекта одной командой
5. сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API. Пример: https://petstore.swagger.io
6. реализовать администраторский Web UI для управления рассылками и получения статистики по отправленным сообщениям (Django admin)
7. обеспечить интеграцию с внешним OAuth2 (VK, Yandex):
```
        /login/vk-oauth2
        /login/yandex-oauth2
```
9. удаленный сервис может быть недоступен, долго отвечать на запросы или выдавать некорректные ответы. Необходимо организовать обработку ошибок и откладывание запросов при неуспехе для последующей повторной отправки. Задержки в работе внешнего сервиса никак не должны оказывать влияние на работу сервиса рассылок.
